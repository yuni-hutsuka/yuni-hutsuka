<!-- Rxyhn's Aesthetic GitHub Profile -->
<div align="justify">

<!-- Profile -->
<p align="center"><strong><samp>❀❀❀❀❀⟰❀❀❀❀❀</samp></strong></p>

<p align="center">
  <samp>
    <b>
      Hello there.
      <br>
      I'm Yuni Hutsuka. Nice to meet you.
    </b>
  </samp>
</p>

<p align="center"><strong><samp>❀❀❀❀❀⟱❀❀❀❀❀</samp></strong></p>

<br>

<h2></h2><br>

<!-- Contact Me -->
<p align="center">
  <samp>
    [<a href="https://twitter.com/yuni_hutsuka">twitter</a>]
    [<a href="https://www.pixiv.net/users/20509498">pixiv</a>]
    [<a href="https://kakuyomu.jp/users/yuni_hutsuka">kakuyomu</a>]
    [<a href="mailto:yuni.wille999@gmail.com">e-mail</a>]
  </samp>
</p>

<!-- About me -->
<p align="center">
  <samp>
    [<a href="./detail/profile.md">About me</a>]
  </samp>
</p>

<!-- My favorite... -->
<p align="center">
  <samp>
    [<a href="./detail/history.md">Favorite anime, manga, novels</a>]
    [<a href="./detail/games.md">Favorite games</a>]
  </samp>
</p>
